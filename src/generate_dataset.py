from keras.models import Sequential
from keras.layers import TimeDistributed, Conv2D, MaxPooling2D, LSTM, Dense, Flatten
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
from numpy import asarray, vstack
from numpy import linspace, empty, reshape, savez_compressed, load
from numpy.random import randint
import os
import collections


def process_npy_file(npy_file,side_length):
    chr = np.load(npy_file)
    image = Image.fromarray(chr,'L')
    image = image.resize((side_length, side_length))
    chr = asarray(image)
    chr = chr.reshape((side_length , side_length, 1))
    return chr


def get_chr_sequence(experiments2replicate , chr, side_length):


    chr_sequence = empty( (len(experiments2replicate.keys()) ,side_length, side_length, 1)   )
    i=0
    for experiment, replicates in experiments2replicate.items():
        chosen_index=randint(0,len(replicates),1)[0]
        chosen_replicate=replicates[ chosen_index ]
        npy_file = os.path.join('/Users/jazberna/HIC_TIMESERIES',experiment+'_'+chosen_replicate,'VC_25kb','chr'+str(chr)+'.npy')
        if not os.path.isfile(npy_file):
            print("could not find {}".format(npy_file))
            exit(1)
        chr_array = process_npy_file(npy_file,side_length)
        chr_sequence[i]=chr_array
        i+=1
    return chr_sequence

def plot_sequence(sequence):
    for i, array in enumerate(sequence):
        plt.subplot(1,sequence.shape[0],i +1)
        plt.imshow(np.reshape(sequence[i],(sequence.shape[1],sequence.shape[1])  ))
    plt.show()

def plot_karyotype_sequence(karyotype_sequence):
    for i in range(karyotype_sequence.shape[0]):
        for j in range(karyotype_sequence.shape[1]):
            plt.subplot( karyotype_sequence.shape[0] ,karyotype_sequence.shape[1], (j + 1 ) + ( karyotype_sequence.shape[1]  * i ) )
            plt.axis('off')
            time_point_array = karyotype_sequence[i,j,:,:,:]
            plt.imshow(np.reshape(time_point_array,(time_point_array.shape[0],time_point_array.shape[1])))
    plt.show()

def generate_dataset(side_length=700):
    experiments2replicate=collections.OrderedDict()
    experiments2replicate['GSE92819']= list(['ENCFF121YPY','ENCFF675SJE','ENCFF740KVX','ENCFF876LAW'])
    experiments2replicate['GSE92793']= list(['ENCFF452FWS','ENCFF525EFN','ENCFF977OQV'])
    experiments2replicate['GSE92804']= list(['ENCFF089KBG','ENCFF378RZT','ENCFF401ZAN','ENCFF939ARM'])
    experiments2replicate['GSE92825']= list(['ENCFF056VLK','ENCFF532DUQ','ENCFF570LWS','ENCFF883YVR'])
    experiments2replicate['GSE92811']= list(['ENCFF219YOB','ENCFF604YDD','ENCFF746AMV','ENCFF845ZEB'])

    chrs = list(range(1,23)) + ['X','Y']

    number_of_karyotype_sequences=50
    karyotype_sequences = empty((len(chrs)* number_of_karyotype_sequences , len(experiments2replicate.keys()), side_length, side_length, 1))
    karyotype_sequences_labels =  empty((len(chrs) * number_of_karyotype_sequences,1))

    for k in range(0, number_of_karyotype_sequences * len(chrs),  len(chrs)):
        karyotype_sequence = empty( ( len(chrs), len(experiments2replicate.keys()), side_length, side_length, 1) )
        karyotype_sequence_labels = empty((len(chrs),1))
        for i, chr in enumerate(chrs):
            seq = get_chr_sequence(experiments2replicate , chr, side_length)
            karyotype_sequence[i]=seq
            karyotype_sequence_labels[i]=i # go from 0 to 23 (instead 1 to 24)
            ##plot_karyotype_sequence(karyotype_sequence)
        #karyotype_sequences[k:k+len(chrs),:,:,:,:]=reshape(karyotype_sequence,( len(chrs),len(experiments2replicate.keys()),side_length,side_length,1))
        karyotype_sequences[k:k+len(chrs),:,:,:,:]=karyotype_sequence
        #print(karyotype_sequences_labels.shape)
        #print(karyotype_sequence_labels.shape)
        karyotype_sequences_labels[k:k+len(chrs),:]=karyotype_sequence_labels

    savez_compressed('hic_time_series_dataset_'+ str(side_length)+'.npz', images=karyotype_sequences , labels=karyotype_sequences_labels)

def get_model(side_length=700):
    # define the model
    model = Sequential()
    model.add(TimeDistributed(Conv2D(2, (2,2), activation='relu'),input_shape=(None,side_length,side_length,1)))
    model.add(TimeDistributed(MaxPooling2D(pool_size=(2, 2))))
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(50))
    model.add(Dense(24, activation='sigmoid'))
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

if __name__ == '__main__':
    side_length=100
    generate_dataset(side_length)
    dataset = load('hic_time_series_dataset.npz')
    #print(dataset['labels'])
    #plot_karyotype_sequence(dataset['images'])
    #model = get_model(side_length)
    #model.summary()
    #model.fit(dataset['images'], dataset['labels'], batch_size=32, epochs=1)