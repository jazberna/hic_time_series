# HiC Time Series using CNN LSTM model
**A proof of concept using data from HiC experiments where a cell line is exposed to Dexamethasone, a drug used to treat COVID-19.**

The motivation for this project is being able to represent 3D HiC based chromosomomes as embeddings, subtract the embeddings for the same chromosome at different experimental conditions and express the extructural changes as the decoded 'subtracted' embedding.

#### The original data was published here:
https://doi.org/10.1016/j.cels.2018.06.007

D'Ippolito AM, McDowell IC, Barrera A, Hong LK, Leichter SM, Bartelt LC, Vockley CM, Majoros WH, Safi A, Song L, Gersbach CA, Crawford GE, Reddy TE. Pre-established Chromatin Interactions Mediate the Genomic Response to Glucocorticoids. Cell Syst. 2018 Aug 22;7(2):146-160.e7. doi: 10.1016/j.cels.2018.06.007. Epub 2018 Jul 18. PMID: 30031775.

#### I got the actual data below from:
http://sysbio.rnet.missouri.edu/3dgenome/GSDB/

### Concept
![concept](images/concept.png)

### training dataset
training dataset created from http://sysbio.rnet.missouri.edu/:
- Hi-C on A549 cell line treated with 100 nM dexamethasone for 0 hours.
- Hi-C on A549 cell line treated with 100 nM dexamethasone for 1 hours.
- Hi-C on A549 cell line treated with 100 nM dexamethasone for 4 hours.
- Hi-C on A549 cell line treated with 100 nM dexamethasone for 8 hours.
- Hi-C on A549 cell line treated with 100 nM dexamethasone for 12 hours.

### code to get training dataset:
https://gitlab.com/jazberna/hic_time_series/-/blob/master/src/generate_dataset.py

The code above needs the VC_25kb/chr*.npy files each of then in a folder such:
    GSE92825_ENCFF883YVR/VC_25kb/chr21.npy

### training dataset:
https://drive.google.com/file/d/1lG4DYyZznn3jKA8RcMuQrTidZcsuT4vR/view?usp=sharing

### training and prediction code:
https://colab.research.google.com/drive/1M7zlqKDw0C967wfOs_rjpVErFX59RHlz?usp=sharing

### model:
https://drive.google.com/file/d/1vCRqbq9RDM6B2aWr629Y_4zkkdA-w-jE/view?usp=sharing